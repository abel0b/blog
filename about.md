---
layout: page
title: About
permalink: /about/
---

## Abel Calluaud

Welcome to my personal tech blog where I like to share about my topics of interest. 

I am a computer science student specialized in high performance computing.

If you would like to get in touch, please leave a comment or drop me a mail at [abel0b@protonmail.com](mailto:abel0b@protonmail.com).

You can be notified of future publications by following me on twitter [@abel0b](https://twitter.com/abel0b).

