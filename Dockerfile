FROM jekyll/jekyll:4 as build-stage
WORKDIR /app
ENV JEKYLL_ENV production
COPY . .
RUN jekyll build --trace

FROM nginx:alpine as production-stage
COPY --from=build-stage /app/_site /usr/share/nginx/html
EXPOSE 80
