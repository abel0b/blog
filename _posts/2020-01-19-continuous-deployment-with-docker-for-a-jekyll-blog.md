---
layout: post
title: "Continuous deployment with Docker for a Jekyll blog"
category: devops
---

As you may have noticed, [this website](https://gitlab.com/abeliam/blog) runs on Jekyll static site generator.
In this post I explain my setup to manage and deploy my Jekyll blog automatically thanks to Docker and Gitlab CI.

## Install docker
If you do not have already installed it, install [`docker`](https://docs.docker.com/install/) and [`docker-compose`](https://docs.docker.com/compose/install/).

You may also want to add your user to the docker group in order to be able to interact with the docker daemon as a simple user.

With `systemd`, you can check whether Docker daemon is running `systemctl status docker`.

## Run Jekyll as a container
The classical way to use Jekyll requires to install `Ruby` and the necessary gems `jekyll` and `bundler`. This method alters your initial configuration, which can lead to version mismatch. If you would want to deal with projects involving various ruby versions, you may also feel the need of a ruby version manager like [rbenv](https://github.com/rbenv/rbenv) or [RVM](http://rvm.io/).

Using docker represent a great alternative because it provides clean and isolated environments. In this post I explain how I managed to use [Jekyll docker images](https://hub.docker.com/r/jekyll/jekyll/) to develop and publish my Jekyll blog.

If you have already a Jekyll blog, move to this directory.
```bash
cd /path/to/my/jekyll/blog
```

Otherwise create a new directory and run the following command to setup a fresh Jekyll blog.
```bash
docker run --rm -it -v $PWD:/srv/jekyll jekyll/jekyll:4 jekyll new .
```
- `--rm` removes the container when stopped
- `-v`/`--volumes` binds directories from host machine to Docker container
- `-it` runs the container in interactive mode

The files have been generated.
```bash
$ ls
404.html about.markdown _config.yml Gemfile Gemfile.lock index.markdown _posts/
```

If you want to start a development server, run.
```bash
docker run --rm -it -v "$PWD:/srv/jekyll" -v "$PWD/vendor/bundle:/usr/local/bundle" -p 4000:4000 jekyll/jekyll:4 jekyll serve
```
- `-p`/`--port` binds ports from host machine to Docker container

The blog is now available on [localhost:4000](http://localhost:4000).


If you would just want to build your blog for production, run the following command.

```bash
docker run --rm -it -v "$PWD:/srv/jekyll" -v "$PWD/vendor/bundle:/usr/local/bundle" --env JEKYLL_ENV=production jekyll/jekyll:4 jekyll build
```

The `_site` directory has been updated with built files.

You may want to save these commands to perform these tasks again later. I used [`rake`](https://github.com/ruby/rake) to do so. I created a Rakefile at the root of my blog with the following content.

```ruby
task :build do
    sh 'docker run --rm -it -v "$PWD:/srv/jekyll" -v "$PWD/vendor/bundle:/usr/local/bundle" --env JEKYLL_ENV=production jekyll/jekyll:4 jekyll build'
end

task :serve do
    sh 'docker run --rm -it -v "$PWD:/srv/jekyll" -v "$PWD/vendor/bundle:/usr/local/bundle" -p 4000:4000 jekyll/jekyll:4 jekyll serve'
end
```
That way, I can build and serve my blog using simple commands `rake build` and `rake serve`.

## Dockerize your Jekyll blog

You can also create a Docker image for your blog in order to deploy it for production. To achieve this, you need to create a [`Dockerfile`](https://github.com/ruby/rake).
The following `Dockerfile` contains instruction about how to build the docker image for my blog. In the first stage, the blog is built using the `jekyll/jekyll` Docker image. Next I chose to deploy the built static files using `nginx` in the next stage.

```dockerfile
FROM jekyll/jekyll:4 as build-stage
ENV JEKYLL_ENV production
WORKDIR /app
COPY . .
RUN jekyll build

FROM nginx:alpine as production-stage
COPY --from=build-stage /app/_site /usr/share/nginx/html
EXPOSE 80
```

To build the docker image.
```bash
docker build -t myblog .
```

If the previous command exited successfully, you can see your newly create image by running `docker images`.

To run this image, run the command
```bash
docker run -p 8080:80 myblog
```
The blog is now running on port 8080.

## Automatic deployment
All these steps can be automated to provide continuous deployment : build the Docker image at each commit and deploy it to a server automatically.

I used [Gitlab CI](http://gitlab.com) to achieve this. But these steps can be adapted for other continuous integration system.

To configure it, create a file `.gitlab-ci.yml` at the root of your project. Add a build stage using the docker:dind image (Docker IN Docker). This stage is responsible for building the docker image and publishing it, to a [docker registry](https://docs.docker.com/registry/). In this case I use the Gitlab registry associated with my repository.

```yaml
stages:
    - build

build:
    stage: build
    image: docker
    services:
        - docker:dind
    before_script:
        - echo $CI_JOB_TOKEN | docker login --username gitlab-ci-token --password-stdin $CI_REGISTRY
    script:
        - mkdir .jekyll-cache _site # avoid permission issue https://github.com/jekyll/jekyll/issues/7591
        - docker pull $CI_REGISTRY_IMAGE || true
        - docker build -t $CI_REGISTRY_IMAGE .
        - docker push $CI_REGISTRY_IMAGE
```

If you commit this file to your GitLab repository, you will see the new image in the section `Packages > Container registry`.

I created a `docker-compose.yml` file at the root of my repository to define the setup of production server.

```bash
version: '3'
services:
    blog:
        image: registry.gitlab.com/abeliam/blog
networks:
    blog:
```

At this point, you should already be able to deploy it on your machine using `docker-compose up` command. Use `-d`/`--detach` option to run the container in background.

Further step is automatic deployment to a server via ssh. To achieve this, define 3 variables in GitLab CI configuration in `Settings > CI/CD > Variables`:
- `DEPLOYER` is the username used for deployment
- `HOST` is the IP address of the server
- `SSH_PRIVATE_KEY` is the ssh private key used for connection to server

Add a deploy stage to your GitLab continuous integration pipeline. Here is how I configured it.
```yaml
deploy:
    stage: deploy
    image: alpine
    before_script:
        - apk add --no-cache openssh
        - mkdir ~/.ssh
        - ssh-keyscan $HOST > ~/.ssh/known_hosts
        - echo "$SSH_PRIVATE_KEY" > server.pem
        - chmod 400 server.pem
    script:
        - cat docker-compose.yml | ssh -i server.pem $DEPLOYER@$HOST "docker pull $CI_REGISTRY_IMAGE && docker-compose -f - up -d"
```

Note: `-f -` option on `docker-compose` means to use the docker-compose configuration given on standard input.

## Conclusion

That's how I managed to use Jekyll as a container and setup continuous deployment. Note that my blog is open-source, then you can find the files referenced on this post can be found on my repository [github.com/abel0b](https://github.com/abel0b).

Feel free to [follow me on twitter](https://twitter.com/abel0b) to be kept in touch of future posts.
