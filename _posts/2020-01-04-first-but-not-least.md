---
layout: post
title: "First but not least"
category: misc
---

Welcome to my blog!
You are currently reading the very first post.

I created this space to express myself and share my knowledge with people having common interests.

My interests in computer science includes high performance computing and compilers.

If you would like to get in touch, please connect with me on twitter [@abel0b](https://twitter.com/abel0b).
